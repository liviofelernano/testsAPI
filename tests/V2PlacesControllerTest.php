<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of V2PlacesControllerTest
 *
 * @author Romain
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V2PlacesControllerTest extends WebTestCase {

    static $idCategorieInseree;
    static $idPlaceInseree;

    /**
     * Création d'une catégorie pour pouvoir dérouler les tests
     */
    public function testCreateCategory() {
        $client = static::createClient();
        $client->request('POST', '/v2/category', [], [], ['HTTP_apikey' => API_KEY], '{"name": "categoriePourTestPlaces"}');
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        // L'ID de catégorie est basé sur un auto_increment, on ne peut pas le pré-supposer
        //$this->assertEquals('{"id":1,"name":"test"}', $client->getResponse()->getContent());
        $reponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals('categoriePourTestPlaces', $reponse->name);
        $this->assertTrue(is_int($reponse->id));

        // Je stocke l'ID de ma catégorie
        self::$idCategorieInseree = $reponse->id;
    }

    /* POST */

    public function testPostPlaceWithoutAuth() {
        // Création du client
        $client = static::createClient();
        // Définition de l'URL à appeler
        $client->request('POST', '/v2/places');
        // Définition des éléments à tester et des valeurs attendues
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testCreateCategory
     */
    public function testPostPlaceWithAuth201() {
        $client = static::createClient();
        $client->request('POST', '/v2/places', [], [], ['HTTP_apikey' => API_KEY], '{"category_id":' . self::$idCategorieInseree . ',"name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}');
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        //$this->assertEquals('{"id":1,"category_id":' . self::$idCategorieInseree . ',"name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}', $client->getResponse()->getContent());
        $reponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(self::$idCategorieInseree, $reponse->category_id);
        $this->assertEquals('test', $reponse->name);
        $this->assertEquals('192 Avenue de la Résistance', $reponse->address);
        $this->assertEquals('38920', $reponse->postal_code);
        $this->assertEquals('Crolles', $reponse->city);
        $this->assertEquals('45.2867291', $reponse->latitude);
        $this->assertEquals('5.8861845', $reponse->longitude);
        $this->assertTrue(is_int($reponse->id));

        // Je stocke l'ID de ma catégorie
        self::$idPlaceInseree = $reponse->id;
    }

    public function testPostPlaceWithAuth400() {
        $client = static::createClient();
        $client->request('POST', '/v2/places', [], [], ['HTTP_apikey' => API_KEY], '{}');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Missing property : category_id or name or address, or postal_code or city"}', $client->getResponse()->getContent());
    }

    public function testPostPlaceWithAuth404() {
        $client = static::createClient();
        $client->request('POST', '/v2/places', [], [], ['HTTP_apikey' => API_KEY], '{"category_id":0,"name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found"}', $client->getResponse()->getContent());
    }

    public function testPostPlaceWithAuth406() {
        $client = static::createClient();
        $client->request('POST', '/v2/places', [], [], ['HTTP_apikey' => API_KEY], '{"category_id":"a","name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}');
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value on category_id"}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth201
     */
    public function testPostPlaceWithAuth409() {
        $client = static::createClient();
        $client->request('POST', '/v2/places', [], [], ['HTTP_apikey' => API_KEY], '{"category_id":' . self::$idCategorieInseree . ',"name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}');
        $this->assertEquals(409, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Already exists"}', $client->getResponse()->getContent());
    }

    /* GET {id} */

    /**
     * @depends testPostPlaceWithAuth201
     */
    public function testGetPlaceIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/' . self::$idPlaceInseree);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth201
     */
    public function testGetPlaceIdWithAuth200() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/' . self::$idPlaceInseree, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"id":' . self::$idPlaceInseree . ',"category_id":' . self::$idCategorieInseree . ',"name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}', $client->getResponse()->getContent());
    }

    public function testGetPlaceIdWithAuth404() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/0', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found"}', $client->getResponse()->getContent());
    }

    public function testGetPlaceIdWithAuth406() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/a', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value"}', $client->getResponse()->getContent());
    }

    /* GET {latitude}/{longitude}/{range} */

    public function testGetPlaceLatLongRangeWithoutAuth401() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/45.2867291/5.8861845/2');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth201
     */
    public function testGetPlaceLatLongRangeWithAuth200() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/45.2867291/5.8861845/2', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"latitude":45.2867291,"longitude":5.8861845,"range":2,"results":[{"id":"' . self::$idPlaceInseree . '","name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":"45.2867291","longitude":"5.8861845","category_id":"' . self::$idCategorieInseree . '","distance":"0"}]}', $client->getResponse()->getContent());
    }

    public function testGetPlaceLatLongRangeWithAuth400() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/9999/9999/2', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid coordinates","latitude":9999,"longitude":9999,"range":2}', $client->getResponse()->getContent());
    }

    public function testGetPlaceLatLongRangeWithAuth406() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/test/5.8861845/2', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","latitude":0,"longitude":5.8861845,"range":2}', $client->getResponse()->getContent());
    }

    public function testGetPlaceLatLongRangeWithAuth416() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/45.2867291/5.8861845/100.1', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(416, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Range too big","latitude":45.2867291,"longitude":5.8861845,"range":100.1}', $client->getResponse()->getContent());
    }

    /* GET category/{id} */

    public function testGetPlaceCategoryWithoutAuth401() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/category/1');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth201
     */
    public function testGetPlaceCategoryWithAuth200() {
        $client = static::createClient();
        $client->request('GET', '/v2/places/category/' . self::$idCategorieInseree, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category_id":' . self::$idCategorieInseree . ',"place":[{"id":"' . self::$idPlaceInseree . '","name":"test","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":"45.2867291","longitude":"5.8861845"}]}', $client->getResponse()->getContent());
    }

//    public function testGetPlaceCategoryWithAuth404() {
//        $client = static::createClient();
//        $client->request('GET', '/v2/places/category/2', [], [], ['HTTP_apikey' => API_KEY]);
//        $this->assertEquals(404, $client->getResponse()->getStatusCode());
//        $this->assertEquals('{"msg":"Not Found","id":"1"}', $client->getResponse()->getContent());
//    }

    /* PATCH {id} */

    /**
     * @depends testPostPlaceWithAuth409
     */
    public function testPatchPlaceIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('PATCH', '/v2/places/' . self::$idPlaceInseree);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth409
     * @depends testGetPlaceIdWithAuth200
     * @depends testGetPlaceLatLongRangeWithAuth200
     * @depends testGetPlaceCategoryWithAuth200
     */
    public function testPatchPlaceIdWithAuth202() {
        $client = static::createClient();
        $client->request('PATCH', '/v2/places/' . self::$idPlaceInseree, [], [], ['HTTP_apikey' => API_KEY], '{"name":"nouveauNom"}');
        $this->assertEquals(202, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"id":' . self::$idPlaceInseree . ',"category_id":' . self::$idCategorieInseree . ',"name":"nouveauNom","address":"192 Avenue de la Résistance","postal_code":"38920","city":"Crolles","latitude":45.2867291,"longitude":5.8861845}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostPlaceWithAuth409
     */
    public function testPatchPlaceIdWithAuth400() {
        $client = static::createClient();
        $client->request('PATCH', '/v2/places/' . self::$idPlaceInseree, [], [], ['HTTP_apikey' => API_KEY], '{"category_id":0}');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid category_id"}', $client->getResponse()->getContent());
    }

//    public function testPatchPlaceIdWithAuth403() {
//        // @TODO
//    }

    public function testPatchPlaceIdWithAuth404() {
        $client = static::createClient();
        $client->request('PATCH', '/v2/places/0', [], [], ['HTTP_apikey' => API_KEY], '{"name":"nouveauNom"}');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found"}', $client->getResponse()->getContent());
    }

//    public function testPatchPlaceIdWithAuth406() {
//        $client = static::createClient();
//        $client->request('PATCH', '/v2/places/1', [], [], ['HTTP_apikey' => API_KEY], '{"longitude": "a"}');
//        $this->assertEquals(406, $client->getResponse()->getStatusCode());
//        $this->assertEquals('{"msg":"Non numeric value"}', $client->getResponse()->getContent());
//    }

    /**
     * @depends testPostPlaceWithAuth409
     */
    public function testPatchPlaceIdWithAuth412() {
        $client = static::createClient();
        $client->request('PATCH', '/v2/places/' . self::$idPlaceInseree, [], [], ['HTTP_apikey' => API_KEY], '{"latitude":"test"}');
        $this->assertEquals(412, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Latitude is not a number"}', $client->getResponse()->getContent());
    }

    /* DELETE {id} */

    /**
     * @depends testPostPlaceWithAuth409
     */
    public function testDeletePlaceIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/places/' . self::$idPlaceInseree);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPatchPlaceIdWithAuth202
     */
    public function testDeletePlaceIdWithAuth200() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/places/' . self::$idPlaceInseree, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('[]', $client->getResponse()->getContent());
    }

//    public function testDeletePlaceIdWithAuth403() {
//        // @TODO
//    }

    public function testDeletePlaceIdWithAuth404() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/places/0', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found"}', $client->getResponse()->getContent());
    }

    public function testDeletePlaceIdWithAuth406() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/places/a', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value"}', $client->getResponse()->getContent());
    }

    /**
     * @depends testDeletePlaceIdWithAuth200
     */
    public function testDeleteCategory() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/category/' . self::$idCategorieInseree, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('[]', $client->getResponse()->getContent());
    }

}
