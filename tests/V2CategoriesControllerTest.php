<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of V2CategoriesControllerTest
 *
 * @author Anael
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V2CategoriesControllerTest extends WebTestCase {

    static $idInsere;

    public function testGetCategoryWithoutAuth() {
        // Création du client
        $client = static::createClient();
        // Définition de l'URL à appeler
        $client->request('GET', '/v2/category');
        // Définition des éléments à tester et des valeurs attendues
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testGetCategoryWithAuth() {
        // Récupération de ma clef d'API
        require __DIR__ . '/config/config.php';

        $client = static::createClient();
        $client->request('GET', '/v2/category', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category":[]}', $client->getResponse()->getContent());
    }

    public function testPostCategoryWithoutAuth() {
        // Création du client
        $client = static::createClient();
        // Définition de l'URL à appeler
        $client->request('POST', '/v2/category');
        // Définition des éléments à tester et des valeurs attendues
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testPostCategoryWithAuth201() {
        $client = static::createClient();
        $client->request('POST', '/v2/category', [], [], ['HTTP_apikey' => API_KEY], '{"name": "test"}');
        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        // L'ID de catégorie est basé sur un auto_increment, on ne peut pas le pré-supposer
        //$this->assertEquals('{"id":1,"name":"test"}', $client->getResponse()->getContent());
        $reponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals('test', $reponse->name);
        $this->assertTrue(is_int($reponse->id));

        // Je stocke l'ID de ma catégorie
        self::$idInsere = $reponse->id;
    }

    public function testPostCategoryWithAuth400() {
        $client = static::createClient();
        $client->request('POST', '/v2/category', [], [], ['HTTP_apikey' => API_KEY], '{}');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"No name provided"}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testPostCategoryWithAuth409() {
        $client = static::createClient();
        $client->request('POST', '/v2/category', [], [], ['HTTP_apikey' => API_KEY], '{"name": "test"}');
        $this->assertEquals(409, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Already exists"}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testGetCategoryIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('GET', '/v2/category/' . self::$idInsere);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testGetCategoryIdWithAuth200() {
        $client = static::createClient();
        $client->request('GET', '/v2/category/' . self::$idInsere, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"id":' . self::$idInsere . ',"name":"test"}', $client->getResponse()->getContent());
    }

    public function testGetCategoryIdWithAuth404() {
        $client = static::createClient();
        $client->request('GET', '/v2/category/0', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"0"}', $client->getResponse()->getContent());
    }

    public function testGetCategoryIdWithAuth406() {
        $client = static::createClient();
        $client->request('GET', '/v2/category/a', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"a"}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testPutCategoryIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('PUT', '/v2/category/' . self::$idInsere);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testPutCategoryIdWithAuth202() {
        $client = static::createClient();
        $client->request('PUT', '/v2/category/' . self::$idInsere, [], [], ['HTTP_apikey' => API_KEY], '{"name": "nouveauNom"}');
        $this->assertEquals(202, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"id":' . self::$idInsere . ',"name":"nouveauNom"}', $client->getResponse()->getContent());
    }

//    public function testPutCategoryIdWithAuth400() {
//        // @TODO
//    }

//    public function testPutCategoryIdWithAuth403() {
//        // @TODO
//    }

    public function testPutCategoryIdWithAuth404() {
        $client = static::createClient();
        $client->request('PUT', '/v2/category/0', [], [], ['HTTP_apikey' => API_KEY], '{"name": "toto"}');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"0"}', $client->getResponse()->getContent());
    }

    public function testPutCategoryIdWithAuth406() {
        $client = static::createClient();
        $client->request('PUT', '/v2/category/a', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"a"}', $client->getResponse()->getContent());
    }

//    public function testPutCategoryIdWithAuth409() {
//        // @TODO
//    }

    /**
     * @depends testPostCategoryWithAuth201
     */
    public function testDeleteCategoryIdWithoutAuth401() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/category/' . self::$idInsere);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    /**
     * @depends testPostCategoryWithAuth201
     * @depends testPostCategoryWithAuth409
     * @depends testGetCategoryIdWithAuth200
     * @depends testPutCategoryIdWithAuth202
     * @depends testDeleteCategoryIdWithoutAuth401
     */
    public function testDeleteCategoryIdWithAuth200() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/category/' . self::$idInsere, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('[]', $client->getResponse()->getContent());
    }

//    public function testDeleteCategoryIdWithAuth403() {
//        // @TODO
//    }

    /**
     * @depends testDeleteCategoryIdWithAuth200
     */
    public function testDeleteCategoryIdWithAuth404() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/category/' . self::$idInsere, [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"' . self::$idInsere . '"}', $client->getResponse()->getContent());
    }

    public function testDeleteCategoryIdWithAuth406() {
        $client = static::createClient();
        $client->request('DELETE', '/v2/category/a', [], [], ['HTTP_apikey' => API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"a"}', $client->getResponse()->getContent());
    }

//    public function testDeleteCategoryIdWithAuth409() {
//        // @TODO
//    }

}
